# local-bigdata-ecosystem

This [docker-compose.yml](https://bitbucket.org/nitesh-kumar-sharma/local-bigdata-ecosystem/src/master/) launches below services in docker platform and runs them in containers in your local host, enabling you to build your own development environments.
[ hadoop, spark, hive, mongodb, postgres-metastore , thrift-server ]

## Why started to create container
Looking for light weighted containers as we all aware that linux-alpine is most light weighted OS just 5.53MB so all containers build under linux-alpine  v3.9


|Service		|Docker Image 																						| Tag 			|SIZE	|
|---------------|---------------------------------------------------------------------------------------------------|---------------|-------|
|Hadoop			|[nikush001/hadoop-ecosystem](https://hub.docker.com/repository/docker/nikush001/hadoop-ecosystem)	| 2.7.4			| 348MB |    
|Spark			|[nikush001/spark](https://hub.docker.com/repository/docker/nikush001/spark)						| 2.4.5			| |				
|Hadoop+Spark+Hive|[nikush001/hadoop-ecosystem](https://hub.docker.com/repository/docker/nikush001/hadoop-ecosystem)| latest		| 1.48GB|
|MongoDB		|[nikush001/mongodb](https://hub.docker.com/repository/docker/nikush001/mongodb)					| 4.0  			| 130MB	|
|Postgres		|[nikush001/postgres](https://hub.docker.com/repository/docker/nikush001/postgres)					| 11.7 			| 36.4MB|

## Prerequisites

* [Docker](https://docs.docker.com/install/)

## How to run  ?
```bash
git clone https://nitesh-kumar-sharma@bitbucket.org/nitesh-kumar-sharma/local-bigdata-ecosystem.git.
cd local-bigdata-ecosystem

For start :
docker-compose up -it (for interactive mode)
docker-compose up -d (for detach mode)

For Stop :
docker-compose down
```

## How to access docker container ?

```
docker ps (it will give you list of running containers)
```
*first column represents the <container_id>*
```
docker exec -it <container_id> bash
```
## Benefit of running container in detach mode
Once you run container in detach mode it allow you to perform more operation on same terminal or cmd-prompt but if you run you container in <-it>  interactive mode your console will be trear as main window so when you try to stop or kill ( CTRL + C ) console it also stop the running container.

## Used docker options

```info
	-d 		: 	For run docker container in detach mode or in background.
	-it 	:	For run docker container in interactive mode.(Default mode when you not specified)
--hostname	:	For specifying the hostname for docker container ( by default docker generate some hashcode as a hostname e.g. de4c035e07e4 )
	-p		:	For open hadoop ports -p <host-port>:<container-port> e.g. -p 9000:9000.
	-v		:	For specifying the volume i.e. <host-dir>:<container-dir> in case you don't want to loss your data at the time of restart.
```

## Exposed ports
|HOST PORT	| CONTAINER PORT	| USE	|
|-----	|--------	|--------	|
|8088	| 8088 |Hadoop UI |
|19270|50070|Web UI to look at current status of HDFS, explore file system
|18080|18080|Spark History Server
|10002|10002|Hive Server2 [hive.server2.webui.port]
|10000|10000|Hive Server2 [hive.server2.thrift.port]
|9083|9083|Thrift server [Hive Metastore]
|5432|5432|postgres


## References

 - https://ambari.apache.org/1.2.3/installing-hadoop-using-ambari/content/reference_chap2_1.html
 - https://www.linode.com/docs/databases/hadoop/how-to-install-and-set-up-hadoop-cluster/

## Contributing
Suggestions are welcome.
[Nitesh Sharma](https://www.linkedin.com/in/niteshsharma01/)